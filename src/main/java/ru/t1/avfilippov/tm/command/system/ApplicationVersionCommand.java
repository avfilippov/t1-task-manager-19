package ru.t1.avfilippov.tm.command.system;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    @Override
    public String getArgument() {
        return "-v";
    }

    @Override
    public String getDescription() {
        return "show application version";
    }

    @Override
    public String getName() {
        return "version";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.19.0");
    }

}
