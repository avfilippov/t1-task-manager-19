package ru.t1.avfilippov.tm.command.system;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @Override
    public String getArgument() {
        return "-a";
    }

    @Override
    public String getDescription() {
        return "show developer's info";
    }

    @Override
    public String getName() {
        return "about";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Alexey Filippov");
        System.out.println("Email: avfilippov@t1-consulting.ru");
    }

}
