package ru.t1.avfilippov.tm.command.task;

import ru.t1.avfilippov.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @Override
    public String getDescription() {
        return "remove task by id";
    }

    @Override
    public String getName() {
        return "task-remove-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        getTaskService().removeById(id);
    }

}
