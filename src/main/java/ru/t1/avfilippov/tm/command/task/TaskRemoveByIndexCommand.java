package ru.t1.avfilippov.tm.command.task;

import ru.t1.avfilippov.tm.util.TerminalUtil;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @Override
    public String getDescription() {
        return "remove task by index";
    }

    @Override
    public String getName() {
        return "task-remove-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getTaskService().removeByIndex(index);
    }

}
