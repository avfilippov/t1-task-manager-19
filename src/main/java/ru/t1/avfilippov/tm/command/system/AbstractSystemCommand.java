package ru.t1.avfilippov.tm.command.system;

import ru.t1.avfilippov.tm.api.service.ICommandService;
import ru.t1.avfilippov.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

}
