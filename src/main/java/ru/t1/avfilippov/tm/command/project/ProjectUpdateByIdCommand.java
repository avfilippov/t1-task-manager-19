package ru.t1.avfilippov.tm.command.project;

import ru.t1.avfilippov.tm.util.TerminalUtil;

public final class ProjectUpdateByIdCommand extends AbstractProjectCommand{

    @Override
    public String getDescription() {
        return "update project by id";
    }

    @Override
    public String getName() {
        return "project-update-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        getProjectService().updateById(id, name, description);
    }

}
