package ru.t1.avfilippov.tm.command.task;

public final class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public String getDescription() {
        return "remove all tasks";
    }

    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        getTaskService().clear();
    }

}
