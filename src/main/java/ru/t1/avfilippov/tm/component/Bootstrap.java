package ru.t1.avfilippov.tm.component;

import ru.t1.avfilippov.tm.api.repository.ICommandRepository;
import ru.t1.avfilippov.tm.api.repository.IProjectRepository;
import ru.t1.avfilippov.tm.api.repository.ITaskRepository;
import ru.t1.avfilippov.tm.api.repository.IUserRepository;
import ru.t1.avfilippov.tm.api.service.*;
import ru.t1.avfilippov.tm.command.AbstractCommand;
import ru.t1.avfilippov.tm.command.project.*;
import ru.t1.avfilippov.tm.command.system.*;
import ru.t1.avfilippov.tm.command.task.*;
import ru.t1.avfilippov.tm.command.user.*;
import ru.t1.avfilippov.tm.enumerated.Role;
import ru.t1.avfilippov.tm.enumerated.Status;
import ru.t1.avfilippov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.avfilippov.tm.exception.system.CommandNotSupportedException;
import ru.t1.avfilippov.tm.model.Project;
import ru.t1.avfilippov.tm.repository.CommandRepository;
import ru.t1.avfilippov.tm.repository.ProjectRepository;
import ru.t1.avfilippov.tm.repository.TaskRepository;
import ru.t1.avfilippov.tm.repository.UserRepository;
import ru.t1.avfilippov.tm.service.*;
import ru.t1.avfilippov.tm.util.TerminalUtil;


public final class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();
    private final ITaskRepository taskRepository = new TaskRepository();
    private final IProjectRepository projectRepository = new ProjectRepository();
    private final IUserRepository userRepository = new UserRepository();

    private final ICommandService commandService = new CommandService(commandRepository);
    private final ILoggerService loggerService = new LoggerService();
    private final IProjectService projectService = new ProjectService(projectRepository);
    private final ITaskService taskService = new TaskService(taskRepository);
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
    private final IUserService userService = new UserService(userRepository);
    private final IAuthService authService = new AuthService(userService);

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    {
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserViewProfileCommand());

        registry(new ApplicationHelpCommand());
        registry(new ApplicationAboutCommand());
        registry(new ApplicationVersionCommand());
        registry(new ApplicationExitCommand());
        registry(new SystemInfoCommand());
        registry(new ArgumentListCommand());
        registry(new CommandListCommand());

        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListCommand());
        registry(new TaskBindToProjectCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByProjectIdCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
    }


    public void run(final String[] args) {
        processArguments(args);
        processCommands();
    }

    public void processCommands() {
        initDemoData();
        initLogger();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                processCommand(command);
                loggerService.command(command);
                System.out.println("[OK]");
            } catch (final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private void initLogger() {
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("*** TASK MANAGER IS SHUTTING DOWN***");
            }
        });
    }

    private void initDemoData() {
        userService.create("TEST USER", "TEST USER", "test@123.ru");
        userService.create("DEMO USER", "DEMO USER", "demo@123.ru");
        userService.create("ADMIN", "ADMIN", Role.ADMIN);

        projectService.add(new Project("Test 2", Status.NOT_STARTED));
        projectService.add(new Project("Test 1", Status.IN_PROGRESS));
        projectService.add(new Project("Test 3", Status.COMPLETED));

        taskService.create("TASK2222", "TASK1 DEMO");
        taskService.create("TASK22", "TASK2 DEMO");
    }

    public void processArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        final String argument = args[0];
        processArgument(argument);
    }


    public void processArgument(final String argument) {
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    public void processCommand(final String command) {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

}
